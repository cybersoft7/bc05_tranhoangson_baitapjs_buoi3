/*
input: Nhập số ngày lương, nhập tiền lương

step: Tiền lương = Số ngày làm việc * số Tiền 1 Ngày

output: Tiền Lương

*/

function tinhLuong() {
  var luong = document.getElementById("txt-luong").value * 1;
  var ngayLam = document.getElementById("txt-SoNgayLam").value * 1;
  var tongLuong = 0;
  tongLuong = luong * ngayLam;
  document.getElementById("result").innerHTML = tongLuong;
}
/*
input: nhập số

step: + Tính Tổng Các Số
       + Tính Trung Bình = Tổng Các Số / 5
       
output= Số Trung Bình       
*/
function tinhTB() {
  var num1 = document.getElementById("txt-num1").value * 1;
  var num2 = document.getElementById("txt-num2").value * 1;
  var num3 = document.getElementById("txt-num3").value * 1;
  var num4 = document.getElementById("txt-num4").value * 1;
  var num5 = document.getElementById("txt-num5").value * 1;
  var tong = num1 + num2 + num3 + num4 + num5;
  var tb = tong / 5;
  document.getElementById("result1").innerHTML = tb;
}
/*
input: nhập số lượng tiền muốn đổi, giá quy đổi

step: + Thành Tiền = Số Lượng * Giá
       
output= Giá Trị Quy Đổi       
*/

function doiTien() {
  var number = document.getElementById("txt-soTien").value * 1;
  var quyDoi = number * 23500;
  document.getElementById("result2").innerHTML = quyDoi;
}
/*
input: nhập chiều dài, chiều rộng

step: + Chu vi = (dài+rộng)*2
      + Diện Tích = dài*Rộng  
       
output: Chu vi, Diện Tích       
*/
function dienTich() {
  var width = document.getElementById("txt-chieuRong").value * 1;
  var length = document.getElementById("txt-chieuDai").value * 1;
  var area = width * length;
  var per = (width + length) * 2;
  document.getElementById(
    "result3"
  ).innerHTML = `DienTich: ${area}, ChuVi: ${per}`;
}
/*
input: nhập số có 2 chữ số

step: + lấy ra số hàng đơn vị
      + lấy ra số hang chục 
       
output:  Tổng = hàng đơn vị + hàng chục    
*/
function tong2KySo() {
  var number = document.getElementById("txt-2kyso").value * 1;
  var donVi = number % 10;
  var hangChuc = Math.floor(number / 10);
  var tong = donVi + hangChuc;
  document.getElementById("result4").innerHTML = tong;
}
